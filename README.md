Takes png image(s) and creates a shadow version of them.

`cargo run myimg.png`
or
`cargo run images/*.png`

The script will create a shadows/ folder and put the output images there.

#### Input
![input image](https://i.imgur.com/y6AzFBk.png)

#### Output
![output image](https://i.imgur.com/cQtBgMN.png)
