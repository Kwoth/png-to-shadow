use std::{
    env::{self},
    fs,
    path::Path,
    time::Instant,
};

use glob::glob;
use image::{GenericImage, GenericImageView};
use palette::{rgb::Rgb, IntoColor, Pixel};
use rand::{Rng, RngCore};
use rayon::iter::{ParallelBridge, ParallelIterator};

fn main() {
    let args = env::args().collect::<Vec<String>>();
    if args.len() != 2 {
        panic!("Only one argument should be passed");
    }

    let shadows_path = Path::new("shadows");
    if shadows_path.exists() {
        if !shadows_path.is_dir() {
            panic!("There is already a shadows file which is not a directory. Please remove it and retry");
        }
    } else {
        let res = fs::create_dir(shadows_path);
        if let Err(reason) = res {
            panic!("Error creating shadows/ directory: {}", reason.to_string())
        }
    }

    let start_time = Instant::now();
    glob(&args[1])
        .expect("Failed to read glob pattern")
        .par_bridge()
        .for_each(|entry| {
            if let Ok(path) = entry {
                
                // read an image and decode it into raw pixels
                let _img = image::io::Reader::open(path.as_path()).unwrap().decode();

                // make sure it goes ok
                if let Ok(mut img) = _img {
                    for x in 0..img.width() {
                        for y in 0..img.height() {
                            // get pixel at position
                            let p = img.get_pixel(x, y);
                            img.put_pixel(x, y, image::Rgba([0, 0, 0, p[3]]))
                        }
                    }

                    // and just write it to the shadows path
                    let out_path = shadows_path.join(path.file_name().unwrap());

                    // println!("Writing {}", out_path.to_str().unwrap());
                    img.save(out_path).unwrap();
                }
            }
        });

    let elapsed = start_time.elapsed();

    println!(
        "{}.{:2>} seconds",
        elapsed.as_secs(),
        elapsed.subsec_nanos() / 100_000
    );
}
